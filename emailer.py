import smtplib
from random import randrange

# Email Variables
SMTP_SERVER = 'smtp.gmail.com'  # Email Server (don't change!)
SMTP_PORT = 587  # Server Port (don't change!)
GMAIL_USERNAME = 'zion.zion.blinks@gmail.com'  # change this to match your gmail account
GMAIL_PASSWORD = 'blinkyblinky'  # change this to match your gmail password
RECIPIANT='zion.zion.blinks@gmail.com'
BODY = ''

def send_email(subject):
    # Create Headers
    headers = ["From: " + GMAIL_USERNAME, "Subject: " + subject + " " + str(randrange(100,999,1)) , "To: " + RECIPIANT,
               "MIME-Version: 1.0", "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    # Connect to Gmail Server
    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    session.ehlo()
    session.starttls()
    session.ehlo()

    # Login to Gmail
    session.login(GMAIL_USERNAME, GMAIL_PASSWORD)

    # Send Email & Exit
    session.sendmail(GMAIL_USERNAME, RECIPIANT, headers + "\r\n\r\n" + BODY)
    session.quit
