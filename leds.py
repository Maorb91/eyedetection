from gpiozero import LED

redLed = LED(26)
greenLed = LED(13)


def red_on():
    redLed.on()


def red_off():
    redLed.off()


def red_blink(on_time=1, off_time=1, number_of_times=1):
    redLed.blink(on_time, off_time, number_of_times)  # on time, off time, number of times


def green_on():
    greenLed.on()


def green_off():
    greenLed.off()


def green_blink(on_time=1, off_time=1, number_of_times=1):
    greenLed.blink(on_time, off_time, number_of_times)  # on time, off time, number of times
