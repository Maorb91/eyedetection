import cv2
import dlib
from scipy.spatial import distance
import time
import numpy as np
import requests
import os.path
import sys

# Positioning of the eyes in the face - do not change
right_eye = range(36, 42)
left_eye = range(42, 48)

detector: callable
predictor: callable


def init_model(path_to_model_file):
    """
      Initializes the shape predictor using the .dat model file
    :param path_to_model_file: path to the shape predictor .dat model file
    :return: the detector and predictor
    """
    global detector, predictor
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(path_to_model_file)
    return detector, predictor


def eye_aspect_ratio(eye):
    """
        Get the eye's aspect ratio, the ratio between the height and the width of the eye.
    :param eye: The landmarks of the eye
    :return: The aspect ratio (Height/width)
    """
    # Measure height on the left part of the eye
    left_top2bottom = distance.euclidean(eye[1], eye[5])
    # Measure height on the right part of the eye
    right_top2bottom = distance.euclidean(eye[2], eye[4])

    # The height we use is the average height of the eye measured on the left and right part of the eye
    average_height = (left_top2bottom + right_top2bottom) / 2.0

    # Measure width
    left2right = distance.euclidean(eye[0], eye[3])

    ear = average_height / left2right
    return ear


def detect(fr):
    """
       Detects face landmarks in the frame
    :param fr: The input frame
    :return: Landmarks array
    """
    # Change the colored frame to grayscale
    g = cv2.cvtColor(fr, cv2.COLOR_BGR2GRAY)

    # Use the detector to detect face in the frame
    f = detector(g)
    if f:
        lm = predictor(g, f[0])
    else:
        lm = []
    return lm


def calc_ear(lm, paint_eyes=False):
    """
        Calculates the eyes aspect ratio, the ratio between each eye's width and height.
    :param lm: The face landmarks
    :return: The EAR for both eyes and their respective convex hulls
    """
    right_eye_vector = np.array(
        [(lm.part(n).x, lm.part(n).y) for n in list(range(right_eye.start, right_eye.stop))])
    left_eye_vector = np.array(
        [(lm.part(n).x, lm.part(n).y) for n in list(range(left_eye.start, left_eye.stop))])

    # Calculate both eyes aspect ratios
    r_ear = eye_aspect_ratio(right_eye_vector)
    l_ear = eye_aspect_ratio(left_eye_vector)

    # Get the convex hull, we won't use it other than painting it on the image
    if paint_eyes:
        left_eye_hull = cv2.convexHull(right_eye_vector)
        right_eye_hull = cv2.convexHull(left_eye_vector)

        return r_ear, l_ear, left_eye_hull, right_eye_hull

    return r_ear, l_ear, None, None


def crop_pic(frame, img_w, img_h, crop_height_offset):
    """
      Crops the center of the frame
    :param frame: The input frame
    :param img_w: The desired width
    :param img_h: The desired height
    :return: a cropped center of the frame
    """
    x = int((len(frame[0]) - img_w) / 2)
    y = int((len(frame) - img_h) * (1 - crop_height_offset) / 2)
    return frame[y:y + img_h, x:x + img_w]


def get_ear(frame, paint_eyes=False):
    """
        Detects landmarks in the picture and returns the average EAR of both eyes
    :param frame: The input frame
    :return: The average EAR of both eyes. If no landmarks found - returns None
    """
    # Detect landmarks (Face, eyes, nose, etc.)
    landmarks = detect(frame)

    # If no face found
    if not landmarks:
        return None

    # if landmarks detected in image
    # start to calculate EAR of right and left eye
    right_ear, left_ear, left_eye_hull, right_eye_hull = calc_ear(landmarks, paint_eyes)

    if paint_eyes and left_eye_hull is not None and right_eye_hull is not None:
        cv2.drawContours(frame, [left_eye_hull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [right_eye_hull], -1, (0, 255, 0), 1)

    return (right_ear + left_ear) / 2.0
