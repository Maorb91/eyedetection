import sms
import emailer
import leds

def alert(msg = "alert!"):
    sms.send_sms(msg)
    #emailer.send_email(msg)
    #red_blink(1, off_time=1, number_of_times=1)
    #buzz()

def leds_booting():
    leds.redLed.blink(0.25, 0.25, 3)
    leds.greenLed.blink(0.25, 0.25, 3)

def leds_eyes_open():
    leds.greenLed.on()
    leds.redLed.off()

def leds_face_not_detected():
    leds.greenLed.off()
    leds.redLed.on()




