from time import time
from models.alarm import Alarm
from models.frame import Frame

# Configuration
number_of_frames_in_memory = 120  # Should cover 4 seconds in a max of 30 FPS
memory_index = 0
ear_threshold = 0.3
percentile_threshold = 0.85
start_time = None
# Closed Eyes frames will be odd, while open eyes steps will be even.
#              C    O     C    O    C    O
alarms = [
    Alarm(1, [1000, 1000, 1000, 1000, 1000], "I need help"),
    Alarm(2, [2000, 2000, 2000, 2000, 2000], "I need water")
]

# End Configuration

# Variables Initialization
memory = [Frame] * number_of_frames_in_memory


def on_frame(ear: float, video_start_time, frame_count):
    """
        This is a main function that should perform on every frame,
        it will invoke all the checks based on the EAR we got on the frame.
    :param ear: The Eye Aspect Ratio from openCV
    """
    global memory
    global memory_index
    global start_time

    now = time()
    # print(int(ear * 100), now)

    # Initialize start time
    if start_time is None:
        start_time = video_start_time

    # Fill the memory array with the EAR and the time of the frame
    memory[memory_index] = Frame(ear, now)

    # TODO: remove now and frame count on production, it's only for debug
    raised_alarms = check_alarm_step(memory, memory_index, now, frame_count)

    # Increment the memory index so the next frame goes into a new index
    memory_index = (memory_index + 1) % number_of_frames_in_memory

    # TODO: Take care of resetting all variables that may inflate
    return raised_alarms


def check_alarm_step(memory, memory_index, time, frame_count):
    """
        Itterates over all the alarm configurations and goes through the frame memory to
        Check if the next step of the sequence is fulfilled.
    :param memory: The memory of all frames EAR's and times
    :param memory_index: The current place in the memory - round robbin
    :param time: The current time
    :return: False if evaluation could not be performed. TODO: change to error
    """

    raised_alarms = []

    for alarm in alarms:
        # TODO: add logic to go back to the beginning of the sequence

        # We only need to check the previous step so we can check maximum 30 FPS
        max_frames_in_current_steps = int(30 * alarm.sequence[alarm.current_step] / 1000)
        frames_scanned = 0
        frames_below_ear_threshold = 0
        ear_sum = 0
        frame = None

        for offset in range(max_frames_in_current_steps):
            frame = memory[(memory_index - offset) % number_of_frames_in_memory]
            # Check if we got to a frame that was created on init and not changed,
            # it means that we don't have enough frames to evaluate
            if frame.time == 0:
                return []

            # Check if we got to the limit of going back in frames - according to step length
            if frame.time < (time - alarm.sequence[alarm.current_step] / 1000):
                break

            # Sum to calculate average
            # TODO: Offset will be enough instead frames_scanned
            frames_scanned = frames_scanned + 1
            ear_sum = ear_sum + frame.ear

            # Check if the ear is below threshold - used for percentile calculation
            if frame.ear < ear_threshold:
                frames_below_ear_threshold = frames_below_ear_threshold + 1

        # Calculate average EAR
        frames_ear_average = ear_sum / frames_scanned

        # Move a step forward if the step requirement is fulfilled
        if check_if_step_triggered(frames_scanned, frames_ear_average, frames_below_ear_threshold, alarm.current_step):
            # TODO: Return this on alarms
            # print('alarm {}: step forward from {} at {}, back to {} {}, average ear: {}'.format(alarm.id, memory_index,
            #                                                                                     memory[
            #                                                                                         memory_index].time,
            #                                                                                     memory_index - offset,
            #                                                                                     frame.time,
            #                                                                                     frames_ear_average))
            print('Alarm {} Step {} at {}, frame: {}'.format(alarm.id, alarm.current_step, time - start_time, frame_count))

            alarm.current_step = alarm.current_step + 1

            # Raise alarm on sequence completion
            if alarm.current_step == len(alarm.sequence):
                raise_alarm(alarm, time - start_time, frame_count)
                alarm.current_step = 0

                raised_alarms.append(alarm)

    return raised_alarms


def check_if_step_triggered(frames_scanned, frames_ear_average, frames_below_ear_threshold, current_step):
    """
        Check if the requirements for the current step of the alarm are fulfilled.
    :param frames_scanned:
    :param frames_ear_average:
    :param frames_below_ear_threshold:
    :param current_step: The current step of the alarm sequence
    :return: True if step is fulfilled. False otherwise
    """
    # Closed Eyes frames will be odd, while open eyes steps will be even.
    is_closed_eyes_step = current_step % 2 == 0

    if is_closed_eyes_step:
        # Check if the average is below the threshold and that at least X% frames are of low EAR
        return frames_ear_average < ear_threshold and frames_below_ear_threshold > (
                frames_scanned * percentile_threshold)

    # Otherwise, check if open eyes step requirements fulfilled
    # Check if the average is above the threshold and that at least X% frames are of high EAR
    return frames_ear_average > ear_threshold and \
           (frames_scanned - frames_below_ear_threshold) > (frames_scanned * percentile_threshold)


def raise_alarm(alarm, time_diff, frame_count):
    alarm.is_raised = True
    print('Alarm {} at {}, frame: {}'.format(alarm.id, time_diff, frame_count))
    # TODO: Raise some-kind of physical alarm
