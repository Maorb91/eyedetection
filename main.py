from time import time
import cv_functions
import cv2
import frame_calculation
import numpy as np  # for normalize
import notifications

# Whether to use a video file or a camera
use_video_file_input = False

# Image crop configuration - do not change
# It helps cropping the center of the picture to reduce
# The open angle to reduce calculations
frame_crop_width = 320
frame_crop_height = 240

video_file_path = './data/1m.mp4'
shape_predictor_model_path = './data/shape_predictor_68_face_landmarks.dat'

debug = True
normalize = True


def analyze_video(video_file_path_=None, normalize_=True, crop_width=320, crop_height=240, crop_height_offset=0.4,
                  debug_=False):
    # Blink leds to indicate booting
    notifications.leds_booting()

    frame_counter = 0

    # Initialize the model
    cv_functions.init_model(shape_predictor_model_path)

    start_time = time()
    # Set capture source
    if video_file_path and use_video_file_input:
        # Use video file as input
        cap = cv2.VideoCapture(video_file_path)

    else:
        # Use camera as input
        cap = cv2.VideoCapture(0)
    # TODO: Check if it works on camera
    video_fps = cap.get(cv2.CAP_PROP_FPS)

    success, frame = cap.read()

    while success or not video_file_path:
        # Crop the frame
        frame = cv_functions.crop_pic(frame, crop_width, crop_height, crop_height_offset)

        if normalize:
            norm_img = np.zeros((crop_height, crop_width))
            norm_img = cv2.normalize(frame, norm_img, 0, 255, cv2.NORM_MINMAX)
            frame = norm_img

        average_ear = cv_functions.get_ear(frame, debug)

        # Show output image
        if video_file_path:
            # Scv2.imshow("DetectBlinks", frame)
            cv2.waitKey(int(1000 / video_fps))
        else:
            cv2.waitKey(1)
        if debug:
            cv2.imshow("DetectBlinks", cv2.resize(frame, (640,480)))

        if average_ear:
            # Use the EAR to detect changes and raise alarms
            raised_alarms = frame_calculation.on_frame(average_ear, time(), frame_counter)
            # Notify for every alarm raised
            for alarm in raised_alarms:
                notifications.alert(alarm.text)
            if len(raised_alarms)>0
                time.sleep(5)
        else:
            notifications.leds_face_not_detected()
            if debug:
                print('No face detected')

        frame_counter = frame_counter + 1
        # Read next frame
        success, frame = cap.read()


if __name__ == '__main__':
    if use_video_file_input:
        analyze_video(debug_=debug, normalize_=normalize, video_file_path_=video_file_path)
    else:
        analyze_video(debug_=debug, normalize_=normalize)
