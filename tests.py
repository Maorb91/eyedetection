import unittest
from random import uniform
from time import sleep

import main
import cv_functions
import frame_calculation

ear_threshold = 0.2


def get_ear_at_second(time):
    # Set video file as input
    cap = main.cv2.VideoCapture(main.video_file_path)
    # Get the specific frame at time
    cap.set(main.cv2.CAP_PROP_POS_MSEC, time * 1000)
    # Read the frame
    success, frame = cap.read()
    # Crop the frame
    frame = cv_functions.crop_pic(frame, main.crop_width, main.crop_height, main.crop_height_offset)
    average_ear = cv_functions.get_ear(frame, True)
    return average_ear


class MyTestCase(unittest.TestCase):

    def test_mock_ears(self):
        for i in range(200):

            # Mimic 10 fps
            fps = 10
            # sleep(.1)
            steps = [500, 300, 1000]
            frames = []
            # Pad with open eyes
            for i in range(10):
                frames.append(0.4)

            for index, step in enumerate(steps):
                for a in range(int(step / fps)):
                    # close eyes frame
                    if index % 2 == 0:
                        frames.append(0.2)
                    else:
                        frames.append(0.4)
            # random_ear = uniform(0, 0.6)

            # Pad with open eyes
            for i in range(10):
                frames.append(0.4)

            for ear in frames:
                # Add 10 percent to the sleep so it will guarantee alarm invocation
                sleep(1.1 / fps)
                frame_calculation.on_frame(ear)
        self.assertEqual(True, True)

    def test_open_eye_frame_ear(self):
        """
            Test an open eye frame
        """
        time = 3
        average_ear = get_ear_at_second(time)
        # Eyes should be open - more then 0.2 ratio
        self.assertGreaterEqual(average_ear, ear_threshold)

    def test_closed_eye_frame_ear(self):
        """
            Test a closed eye frame
        """
        time = 15
        average_ear = get_ear_at_second(time)
        # Eyes should be open - more then 0.2 ratio
        self.assertLessEqual(average_ear, ear_threshold)

    def test_with_m1_video(self):
        # Array of frame numbers and if the eye is closed or open
        # This is a ground truth description of the video
        frames_with_eyes_closed = [(119, 121), (202, 205), (229, 285), (372, 432), (485, 535), (568, 572), (615, 690),
                                   (758, 786), (810, 839), (870, 900), (995, 1233), (1305, 1342), (1381, 1435),
                                   (1470, 1514), (1552, 1583), (1620, 1654)]




if __name__ == '__main__':
    unittest.main()
