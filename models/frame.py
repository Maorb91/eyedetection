class Frame:
    # Holds the EAR and time of each frame
    ear = 0
    time = 0

    def __init__(self, f_ear=0.0, f_time=0.0):
        self.ear = f_ear
        self.time = f_time