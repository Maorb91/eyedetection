class Alarm:
    # Holds the sequence to raise the alarm and keeps information about
    # The current state of the alarm
    sequence = []
    current_step = 0
    text = ""
    id = 0
    is_raised = False

    def __init__(self, a_id ,a_sequence, a_type):
        self.sequence = a_sequence
        self.text = a_type
        self.id = a_id
